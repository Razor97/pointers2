#include <iostream>

using namespace std;


int main()
{
  int x = 25, y = 2 * x;    
  auto a = &x, b = a;
  auto c = *a, d = 2 * *b;


  cout<<"x = "<<x<<", y = "<<y<<endl;
  cout<<"x = "<<a<<", y = "<<&y<<endl;


  cout<<"a = "<<*a<<", b = "<<*b<<", c = "<<c<<", d = "<<d<<endl;
  cout<<"a = "<<&a<<",b = "<<&b<<", c = "<<&c<<", d = "<<&d<<endl;

 



}
